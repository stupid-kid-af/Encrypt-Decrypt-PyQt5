
# Encrypt-Decrypt-PyQt5

A solution for encrypting and decrypting files built with PyQt5

## Run Locally

Clone the project

```bash
  git clone https://github.com/stupid-kid-af/Encrypt-Decrypt-PyQt5.git
```

Go to the project directory

```bash
  cd Encrypt-Decrypt-PyQt5
```

Install dependencies

```bash
  pip install PyQt5
```


  
## Roadmap

- Additional platform support

- Add more integrations

  
## Support

For support, email pushpatripath1111@gmail.com or join our Telegram channel (Soon).

  
